import { Router } from 'express';
import Player from '../models/player';

const router = Router();

router.get('/players', async (req, res) => {
    let playerModel = req.app['models'].player;
    let players = await playerModel.all();
    res.json(players);
});

router.post('/players', async (req, res) => {
    let name = req.body.name;
    let playerModel = req.app['models'].player;
    let last_id = await playerModel.create(name);
    res.json({ message: 'Player was created', id: last_id, name });
});

router.get('/players/:id', async (req, res) => {
    let playerModel = req.app['models'].player;
    let player = await playerModel.get(req.params['id']);
    res.json(player);
});

router.get('/players/:id/hand', async (req, res) => {
    let playerModel = req.app['models'].player;
    let player = await playerModel.get(req.params['id']);
    res.json(player);
});

router.post('/players/:id/hand', async (req, res) => {
    let player_id = req.params['id'];
    let cards = req.body.cards;
    let playerModel = req.app['models'].player;

    res.json({ message: 'Player hand was updated', player_id, cards});
});

export default router;