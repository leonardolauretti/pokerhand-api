export default class Player {
    db = null;

    constructor(db) {
        this.db = db;
    }

    async create(name) {
        let created = await this.db.run('INSERT INTO players (name) VALUES (?)', [name]);
        return created.lastID;
    }

    async all() {
        let rows = await this.db.all('SELECT * FROM players');
        return rows;
    }

    async get(id) {
        const result = await this.db.get('SELECT * FROM players WHERE id = ?', id);
        return result;
    }
}