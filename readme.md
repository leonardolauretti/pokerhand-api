# Database
The chosen database for the project was SQLite. One of the reasons for the choice is the ease of application testing and portability through a file.

# API Testing
A collection was created in Postman to test each endpoint of the developed API. The collection can be accessed through the following URL: https://www.getpostman.com/collections/d71b7af44f6cc01199c5
Don't forget to download and install Postman before importing the collection.

# Run project
To run the project, just run the npm run serve command in the main project directory.