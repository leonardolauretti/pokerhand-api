import express from 'express';
import sqlite3 from 'sqlite3';
import { open } from 'sqlite';

var bodyParser = require('body-parser');

import apiRouter from './routes/api';

// Models
import Player from './models/player';

const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended: true }));

(async () => {
    const database = await open({
        filename: './database/database.sqlite',
        driver: sqlite3.Database,
    });

    let db = database.db
    app['db'] = db;

    // Create players table
    await database.exec('CREATE TABLE IF NOT EXISTS players (id integer primary key, name text)');

    // Create hands table
    await database.exec('CREATE TABLE IF NOT EXISTS hands (id integer primary key, player_id integer, cards text)')

    // Register models
    app['models'] = {
        'player': new Player(database),
    };
})();

app.use('/api', apiRouter);

app.listen(port, () => {
    console.log(`Application is running on port ${port}.`);
});